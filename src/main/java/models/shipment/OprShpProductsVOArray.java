package models.shipment;

/**
 * Created by Wiktor Szymanowski
 * Date: 27.03.2020
 */
public class OprShpProductsVOArray {

    private String addServiceFlag;
    private String prodId;
    private String serviceDate;
    private String compProdFlag;
    private String contractId;
    private String payerContractId;
    private String oprProdStatusFlag;
    private String discountSurchargeFlag;
    private String notAffectServiceCode;

    public OprShpProductsVOArray(){};

    public OprShpProductsVOArray(String addServiceFlag, String prodId, String serviceDate, String compProdFlag, String contractId, String payerContractId, String oprProdStatusFlag, String discountSurchargeFlag, String notAffectServiceCode) {
        this.addServiceFlag = addServiceFlag;
        this.prodId = prodId;
        this.serviceDate = serviceDate;
        this.compProdFlag = compProdFlag;
        this.contractId = contractId;
        this.payerContractId = payerContractId;
        this.oprProdStatusFlag = oprProdStatusFlag;
        this.discountSurchargeFlag = discountSurchargeFlag;
        this.notAffectServiceCode = notAffectServiceCode;
    }

    public String getAddServiceFlag() {
        return addServiceFlag;
    }

    public void setAddServiceFlag(String addServiceFlag) {
        this.addServiceFlag = addServiceFlag;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getCompProdFlag() {
        return compProdFlag;
    }

    public void setCompProdFlag(String compProdFlag) {
        this.compProdFlag = compProdFlag;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getPayerContractId() {
        return payerContractId;
    }

    public void setPayerContractId(String payerContractId) {
        this.payerContractId = payerContractId;
    }

    public String getOprProdStatusFlag() {
        return oprProdStatusFlag;
    }

    public void setOprProdStatusFlag(String oprProdStatusFlag) {
        this.oprProdStatusFlag = oprProdStatusFlag;
    }

    public String getDiscountSurchargeFlag() {
        return discountSurchargeFlag;
    }

    public void setDiscountSurchargeFlag(String discountSurchargeFlag) {
        this.discountSurchargeFlag = discountSurchargeFlag;
    }

    public String getNotAffectServiceCode() {
        return notAffectServiceCode;
    }

    public void setNotAffectServiceCode(String notAffectServiceCode) {
        this.notAffectServiceCode = notAffectServiceCode;
    }
}
