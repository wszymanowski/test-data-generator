package models.shipment;

import java.util.ArrayList;

/**
 * Created by Wiktor Szymanowski
 * Date: 27.03.2020
 */
public class OprShipmentVO {

    private ArrayList<OprShpParcelVOArray> oprShpParcelVOArray;
    private ArrayList<OprShpProductsVOArray> oprShpProductsVOArray;
    private String contractId;
    private String shpDate;
    private String shpTime;
    private String compId;
    private String payerCustId;
    private String payerAddressId;
    private String payerCountryId;
    private String senderCustId;
    private String senderAdrId;
    private String senderZipCode;
    private String senderCountryId;
    private String senderAreaId;
    private String senderZoneId;
    private String senderCityId;
    private String receiverCountryId;
    private String receiverZipCode;
    private String receiverAreaId;
    private String receiverZoneId;
    private String receiverCityId;
    private String receiverCity;
    private String receiverName;
    private String receiverAddressText;
    private String receiverTelNo;
    private String receiverHouseNo;
    private String receiverCustType;
    private String pieceNumber;
    private String totalKg;
    private String totalDesi;
    private String totalDesiKg;
    private String collectionType;
    private String shipmentType;
    private String shpMainProductId;
    private String mainServiceCode;
    private String departureTourNumber;
    private String deliveryUnitId;
    private String departureUnitId;
    private String pickupType;
    private String appOriginType;
    private String ssaCustProfileId;
    private String psCountryId;
    private String receiverSubzoneId;
    private String senderSubzoneId;
    private String receiverReferenceNumber;
    private String receiverEmailAddress;
    private String receiverTelArea;
    private String receiverTownId;
    private String ssaUserId;
    private String receiverWorkAreaId;
    private String returnAddressId;
    private OprShipmentAddInfoVO oprShipmentAddInfoVO;
    private String seurErrorCode;
    private String numberOfCopies;

    public OprShipmentVO(){};

    public OprShipmentVO(ArrayList<OprShpParcelVOArray> oprShpParcelVOArray, ArrayList<OprShpProductsVOArray> oprShpProductsVOArray, String contractId, String shpDate, String shpTime, String compId, String payerCustId, String payerAddressId, String payerCountryId, String senderCustId, String senderAdrId, String senderZipCode, String senderCountryId, String senderAreaId, String senderZoneId, String senderCityId, String receiverCountryId, String receiverZipCode, String receiverAreaId, String receiverZoneId, String receiverCityId, String receiverCity, String receiverName, String receiverAddressText, String receiverTelNo, String receiverHouseNo, String receiverCustType, String pieceNumber, String totalKg, String totalDesi, String totalDesiKg, String collectionType, String shipmentType, String shpMainProductId, String mainServiceCode, String departureTourNumber, String deliveryUnitId, String departureUnitId, String pickupType, String appOriginType, String ssaCustProfileId, String psCountryId, String receiverSubzoneId, String senderSubzoneId, String receiverReferenceNumber, String receiverEmailAddress, String receiverTelArea, String receiverTownId, String ssaUserId, String receiverWorkAreaId, String returnAddressId, OprShipmentAddInfoVO oprShipmentAddInfoVO, String seurErrorCode, String numberOfCopies) {
        this.oprShpParcelVOArray = oprShpParcelVOArray;
        this.oprShpProductsVOArray = oprShpProductsVOArray;
        this.contractId = contractId;
        this.shpDate = shpDate;
        this.shpTime = shpTime;
        this.compId = compId;
        this.payerCustId = payerCustId;
        this.payerAddressId = payerAddressId;
        this.payerCountryId = payerCountryId;
        this.senderCustId = senderCustId;
        this.senderAdrId = senderAdrId;
        this.senderZipCode = senderZipCode;
        this.senderCountryId = senderCountryId;
        this.senderAreaId = senderAreaId;
        this.senderZoneId = senderZoneId;
        this.senderCityId = senderCityId;
        this.receiverCountryId = receiverCountryId;
        this.receiverZipCode = receiverZipCode;
        this.receiverAreaId = receiverAreaId;
        this.receiverZoneId = receiverZoneId;
        this.receiverCityId = receiverCityId;
        this.receiverCity = receiverCity;
        this.receiverName = receiverName;
        this.receiverAddressText = receiverAddressText;
        this.receiverTelNo = receiverTelNo;
        this.receiverHouseNo = receiverHouseNo;
        this.receiverCustType = receiverCustType;
        this.pieceNumber = pieceNumber;
        this.totalKg = totalKg;
        this.totalDesi = totalDesi;
        this.totalDesiKg = totalDesiKg;
        this.collectionType = collectionType;
        this.shipmentType = shipmentType;
        this.shpMainProductId = shpMainProductId;
        this.mainServiceCode = mainServiceCode;
        this.departureTourNumber = departureTourNumber;
        this.deliveryUnitId = deliveryUnitId;
        this.departureUnitId = departureUnitId;
        this.pickupType = pickupType;
        this.appOriginType = appOriginType;
        this.ssaCustProfileId = ssaCustProfileId;
        this.psCountryId = psCountryId;
        this.receiverSubzoneId = receiverSubzoneId;
        this.senderSubzoneId = senderSubzoneId;
        this.receiverReferenceNumber = receiverReferenceNumber;
        this.receiverEmailAddress = receiverEmailAddress;
        this.receiverTelArea = receiverTelArea;
        this.receiverTownId = receiverTownId;
        this.ssaUserId = ssaUserId;
        this.receiverWorkAreaId = receiverWorkAreaId;
        this.returnAddressId = returnAddressId;
        this.oprShipmentAddInfoVO = oprShipmentAddInfoVO;
        this.seurErrorCode = seurErrorCode;
        this.numberOfCopies = numberOfCopies;
    }

    public ArrayList<OprShpParcelVOArray> getOprShpParcelVOArray() {
        return oprShpParcelVOArray;
    }

    public void setOprShpParcelVOArray(ArrayList<OprShpParcelVOArray> oprShpParcelVOArray) {
        this.oprShpParcelVOArray = oprShpParcelVOArray;
    }

    public ArrayList<OprShpProductsVOArray> getOprShpProductsVOArray() {
        return oprShpProductsVOArray;
    }

    public void setOprShpProductsVOArray(ArrayList<OprShpProductsVOArray> oprShpProductsVOArray) {
        this.oprShpProductsVOArray = oprShpProductsVOArray;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getShpDate() {
        return shpDate;
    }

    public void setShpDate(String shpDate) {
        this.shpDate = shpDate;
    }

    public String getShpTime() {
        return shpTime;
    }

    public void setShpTime(String shpTime) {
        this.shpTime = shpTime;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getPayerCustId() {
        return payerCustId;
    }

    public void setPayerCustId(String payerCustId) {
        this.payerCustId = payerCustId;
    }

    public String getPayerAddressId() {
        return payerAddressId;
    }

    public void setPayerAddressId(String payerAddressId) {
        this.payerAddressId = payerAddressId;
    }

    public String getPayerCountryId() {
        return payerCountryId;
    }

    public void setPayerCountryId(String payerCountryId) {
        this.payerCountryId = payerCountryId;
    }

    public String getSenderCustId() {
        return senderCustId;
    }

    public void setSenderCustId(String senderCustId) {
        this.senderCustId = senderCustId;
    }

    public String getSenderAdrId() {
        return senderAdrId;
    }

    public void setSenderAdrId(String senderAdrId) {
        this.senderAdrId = senderAdrId;
    }

    public String getSenderZipCode() {
        return senderZipCode;
    }

    public void setSenderZipCode(String senderZipCode) {
        this.senderZipCode = senderZipCode;
    }

    public String getSenderCountryId() {
        return senderCountryId;
    }

    public void setSenderCountryId(String senderCountryId) {
        this.senderCountryId = senderCountryId;
    }

    public String getSenderAreaId() {
        return senderAreaId;
    }

    public void setSenderAreaId(String senderAreaId) {
        this.senderAreaId = senderAreaId;
    }

    public String getSenderZoneId() {
        return senderZoneId;
    }

    public void setSenderZoneId(String senderZoneId) {
        this.senderZoneId = senderZoneId;
    }

    public String getSenderCityId() {
        return senderCityId;
    }

    public void setSenderCityId(String senderCityId) {
        this.senderCityId = senderCityId;
    }

    public String getReceiverCountryId() {
        return receiverCountryId;
    }

    public void setReceiverCountryId(String receiverCountryId) {
        this.receiverCountryId = receiverCountryId;
    }

    public String getReceiverZipCode() {
        return receiverZipCode;
    }

    public void setReceiverZipCode(String receiverZipCode) {
        this.receiverZipCode = receiverZipCode;
    }

    public String getReceiverAreaId() {
        return receiverAreaId;
    }

    public void setReceiverAreaId(String receiverAreaId) {
        this.receiverAreaId = receiverAreaId;
    }

    public String getReceiverZoneId() {
        return receiverZoneId;
    }

    public void setReceiverZoneId(String receiverZoneId) {
        this.receiverZoneId = receiverZoneId;
    }

    public String getReceiverCityId() {
        return receiverCityId;
    }

    public void setReceiverCityId(String receiverCityId) {
        this.receiverCityId = receiverCityId;
    }

    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverAddressText() {
        return receiverAddressText;
    }

    public void setReceiverAddressText(String receiverAddressText) {
        this.receiverAddressText = receiverAddressText;
    }

    public String getReceiverTelNo() {
        return receiverTelNo;
    }

    public void setReceiverTelNo(String receiverTelNo) {
        this.receiverTelNo = receiverTelNo;
    }

    public String getReceiverHouseNo() {
        return receiverHouseNo;
    }

    public void setReceiverHouseNo(String receiverHouseNo) {
        this.receiverHouseNo = receiverHouseNo;
    }

    public String getReceiverCustType() {
        return receiverCustType;
    }

    public void setReceiverCustType(String receiverCustType) {
        this.receiverCustType = receiverCustType;
    }

    public String getPieceNumber() {
        return pieceNumber;
    }

    public void setPieceNumber(String pieceNumber) {
        this.pieceNumber = pieceNumber;
    }

    public String getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(String totalKg) {
        this.totalKg = totalKg;
    }

    public String getTotalDesi() {
        return totalDesi;
    }

    public void setTotalDesi(String totalDesi) {
        this.totalDesi = totalDesi;
    }

    public String getTotalDesiKg() {
        return totalDesiKg;
    }

    public void setTotalDesiKg(String totalDesiKg) {
        this.totalDesiKg = totalDesiKg;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public String getShpMainProductId() {
        return shpMainProductId;
    }

    public void setShpMainProductId(String shpMainProductId) {
        this.shpMainProductId = shpMainProductId;
    }

    public String getMainServiceCode() {
        return mainServiceCode;
    }

    public void setMainServiceCode(String mainServiceCode) {
        this.mainServiceCode = mainServiceCode;
    }

    public String getDepartureTourNumber() {
        return departureTourNumber;
    }

    public void setDepartureTourNumber(String departureTourNumber) {
        this.departureTourNumber = departureTourNumber;
    }

    public String getDeliveryUnitId() {
        return deliveryUnitId;
    }

    public void setDeliveryUnitId(String deliveryUnitId) {
        this.deliveryUnitId = deliveryUnitId;
    }

    public String getDepartureUnitId() {
        return departureUnitId;
    }

    public void setDepartureUnitId(String departureUnitId) {
        this.departureUnitId = departureUnitId;
    }

    public String getPickupType() {
        return pickupType;
    }

    public void setPickupType(String pickupType) {
        this.pickupType = pickupType;
    }

    public String getAppOriginType() {
        return appOriginType;
    }

    public void setAppOriginType(String appOriginType) {
        this.appOriginType = appOriginType;
    }

    public String getSsaCustProfileId() {
        return ssaCustProfileId;
    }

    public void setSsaCustProfileId(String ssaCustProfileId) {
        this.ssaCustProfileId = ssaCustProfileId;
    }

    public String getPsCountryId() {
        return psCountryId;
    }

    public void setPsCountryId(String psCountryId) {
        this.psCountryId = psCountryId;
    }

    public String getReceiverSubzoneId() {
        return receiverSubzoneId;
    }

    public void setReceiverSubzoneId(String receiverSubzoneId) {
        this.receiverSubzoneId = receiverSubzoneId;
    }

    public String getSenderSubzoneId() {
        return senderSubzoneId;
    }

    public void setSenderSubzoneId(String senderSubzoneId) {
        this.senderSubzoneId = senderSubzoneId;
    }

    public String getReceiverReferenceNumber() {
        return receiverReferenceNumber;
    }

    public void setReceiverReferenceNumber(String receiverReferenceNumber) {
        this.receiverReferenceNumber = receiverReferenceNumber;
    }

    public String getReceiverEmailAddress() {
        return receiverEmailAddress;
    }

    public void setReceiverEmailAddress(String receiverEmailAddress) {
        this.receiverEmailAddress = receiverEmailAddress;
    }

    public String getReceiverTelArea() {
        return receiverTelArea;
    }

    public void setReceiverTelArea(String receiverTelArea) {
        this.receiverTelArea = receiverTelArea;
    }

    public String getReceiverTownId() {
        return receiverTownId;
    }

    public void setReceiverTownId(String receiverTownId) {
        this.receiverTownId = receiverTownId;
    }

    public String getSsaUserId() {
        return ssaUserId;
    }

    public void setSsaUserId(String ssaUserId) {
        this.ssaUserId = ssaUserId;
    }

    public String getReceiverWorkAreaId() {
        return receiverWorkAreaId;
    }

    public void setReceiverWorkAreaId(String receiverWorkAreaId) {
        this.receiverWorkAreaId = receiverWorkAreaId;
    }

    public String getReturnAddressId() {
        return returnAddressId;
    }

    public void setReturnAddressId(String returnAddressId) {
        this.returnAddressId = returnAddressId;
    }

    public OprShipmentAddInfoVO getOprShipmentAddInfoVO() {
        return oprShipmentAddInfoVO;
    }

    public void setOprShipmentAddInfoVO(OprShipmentAddInfoVO oprShipmentAddInfoVO) {
        this.oprShipmentAddInfoVO = oprShipmentAddInfoVO;
    }

    public String getSeurErrorCode() {
        return seurErrorCode;
    }

    public void setSeurErrorCode(String seurErrorCode) {
        this.seurErrorCode = seurErrorCode;
    }

    public String getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(String numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }
}
