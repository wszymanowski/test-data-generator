package models.shipment;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Wiktor Szymanowski
 * Date: 27.03.2020
 */
@XmlRootElement(name="saveShipment")
public class Shipment {

    private String wsUserName;
    private String wsPassword;
    private String wsLang;
    private String applicationType;
    private OprShipmentVO oprShipmentVO;

    public Shipment(){};

    public Shipment(String wsUserName, String wsPassword, String wsLang, String applicationType, OprShipmentVO oprShipmentVO) {
        super();
        this.wsUserName = wsUserName;
        this.wsPassword = wsPassword;
        this.wsLang = wsLang;
        this.applicationType = applicationType;
        this.oprShipmentVO = oprShipmentVO;
    }

    public String getWsUserName() {
        return wsUserName;
    }

    public void setWsUserName(String wsUserName) {
        this.wsUserName = wsUserName;
    }

    public String getWsPassword() {
        return wsPassword;
    }

    public void setWsPassword(String wsPassword) {
        this.wsPassword = wsPassword;
    }

    public String getWsLang() {
        return wsLang;
    }

    public void setWsLang(String wsLang) {
        this.wsLang = wsLang;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public OprShipmentVO getOprShipmentVO() {
        return oprShipmentVO;
    }

    public void setOprShipmentVO(OprShipmentVO oprShipmentVO) {
        this.oprShipmentVO = oprShipmentVO;
    }
}
