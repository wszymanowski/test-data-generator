package models.shipment;

/**
 * Created by Wiktor Szymanowski
 * Date: 27.03.2020
 */
public class OprShipmentAddInfoVO {

    private String contractType;
    private String realDepotId;
    private String relatedBusinessUnitId;
    private String barcodeOneDimPrint;
    private String senderStreet;
    private String senderHouseNo;
    private String receiverLastName;
    private String receiverFirstName;
    private String receiverTownName;
    private String receiverGsmArea;
    private String receiverGsmNumber;
    private String routeDsort;
    private String senderTownId;
    private String senderCompName;
    private String routeOsort;
    private String workBranchUnitId;
    private String senderLegacyId;

    public OprShipmentAddInfoVO(){};

    public OprShipmentAddInfoVO(String contractType, String realDepotId, String relatedBusinessUnitId, String barcodeOneDimPrint, String senderStreet, String senderHouseNo, String receiverLastName, String receiverFirstName, String receiverTownName, String receiverGsmArea, String receiverGsmNumber, String routeDsort, String senderTownId, String senderCompName, String routeOsort, String workBranchUnitId, String senderLegacyId) {
        super();
        this.contractType = contractType;
        this.realDepotId = realDepotId;
        this.relatedBusinessUnitId = relatedBusinessUnitId;
        this.barcodeOneDimPrint = barcodeOneDimPrint;
        this.senderStreet = senderStreet;
        this.senderHouseNo = senderHouseNo;
        this.receiverLastName = receiverLastName;
        this.receiverFirstName = receiverFirstName;
        this.receiverTownName = receiverTownName;
        this.receiverGsmArea = receiverGsmArea;
        this.receiverGsmNumber = receiverGsmNumber;
        this.routeDsort = routeDsort;
        this.senderTownId = senderTownId;
        this.senderCompName = senderCompName;
        this.routeOsort = routeOsort;
        this.workBranchUnitId = workBranchUnitId;
        this.senderLegacyId = senderLegacyId;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getRealDepotId() {
        return realDepotId;
    }

    public void setRealDepotId(String realDepotId) {
        this.realDepotId = realDepotId;
    }

    public String getRelatedBusinessUnitId() {
        return relatedBusinessUnitId;
    }

    public void setRelatedBusinessUnitId(String relatedBusinessUnitId) {
        this.relatedBusinessUnitId = relatedBusinessUnitId;
    }

    public String getBarcodeOneDimPrint() {
        return barcodeOneDimPrint;
    }

    public void setBarcodeOneDimPrint(String barcodeOneDimPrint) {
        this.barcodeOneDimPrint = barcodeOneDimPrint;
    }

    public String getSenderStreet() {
        return senderStreet;
    }

    public void setSenderStreet(String senderStreet) {
        this.senderStreet = senderStreet;
    }

    public String getSenderHouseNo() {
        return senderHouseNo;
    }

    public void setSenderHouseNo(String senderHouseNo) {
        this.senderHouseNo = senderHouseNo;
    }

    public String getReceiverLastName() {
        return receiverLastName;
    }

    public void setReceiverLastName(String receiverLastName) {
        this.receiverLastName = receiverLastName;
    }

    public String getReceiverFirstName() {
        return receiverFirstName;
    }

    public void setReceiverFirstName(String receiverFirstName) {
        this.receiverFirstName = receiverFirstName;
    }

    public String getReceiverTownName() {
        return receiverTownName;
    }

    public void setReceiverTownName(String receiverTownName) {
        this.receiverTownName = receiverTownName;
    }

    public String getReceiverGsmArea() {
        return receiverGsmArea;
    }

    public void setReceiverGsmArea(String receiverGsmArea) {
        this.receiverGsmArea = receiverGsmArea;
    }

    public String getReceiverGsmNumber() {
        return receiverGsmNumber;
    }

    public void setReceiverGsmNumber(String receiverGsmNumber) {
        this.receiverGsmNumber = receiverGsmNumber;
    }

    public String getRouteDsort() {
        return routeDsort;
    }

    public void setRouteDsort(String routeDsort) {
        this.routeDsort = routeDsort;
    }

    public String getSenderTownId() {
        return senderTownId;
    }

    public void setSenderTownId(String senderTownId) {
        this.senderTownId = senderTownId;
    }

    public String getSenderCompName() {
        return senderCompName;
    }

    public void setSenderCompName(String senderCompName) {
        this.senderCompName = senderCompName;
    }

    public String getRouteOsort() {
        return routeOsort;
    }

    public void setRouteOsort(String routeOsort) {
        this.routeOsort = routeOsort;
    }

    public String getWorkBranchUnitId() {
        return workBranchUnitId;
    }

    public void setWorkBranchUnitId(String workBranchUnitId) {
        this.workBranchUnitId = workBranchUnitId;
    }

    public String getSenderLegacyId() {
        return senderLegacyId;
    }

    public void setSenderLegacyId(String senderLegacyId) {
        this.senderLegacyId = senderLegacyId;
    }
}
