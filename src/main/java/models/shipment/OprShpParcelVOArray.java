package models.shipment;

/**
 * Created by Wiktor Szymanowski
 * Date: 27.03.2020
 */
public class OprShpParcelVOArray {

    private String goodsPrice;
    private String parcelType;
    private String kg;
    private String desi;
    private String length;
    private String width;
    private String height;
    private String printStatus;
    private String hinsAmount;
    private String desiKg;
    private String parcelCount;
    private String serviceCode;

    public OprShpParcelVOArray(){};

    public OprShpParcelVOArray(String goodsPrice, String parcelType, String kg, String desi, String length, String width, String height, String printStatus, String hinsAmount, String desiKg, String parcelCount, String serviceCode) {
        this.goodsPrice = goodsPrice;
        this.parcelType = parcelType;
        this.kg = kg;
        this.desi = desi;
        this.length = length;
        this.width = width;
        this.height = height;
        this.printStatus = printStatus;
        this.hinsAmount = hinsAmount;
        this.desiKg = desiKg;
        this.parcelCount = parcelCount;
        this.serviceCode = serviceCode;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getParcelType() {
        return parcelType;
    }

    public void setParcelType(String parcelType) {
        this.parcelType = parcelType;
    }

    public String getKg() {
        return kg;
    }

    public void setKg(String kg) {
        this.kg = kg;
    }

    public String getDesi() {
        return desi;
    }

    public void setDesi(String desi) {
        this.desi = desi;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(String printStatus) {
        this.printStatus = printStatus;
    }

    public String getHinsAmount() {
        return hinsAmount;
    }

    public void setHinsAmount(String hinsAmount) {
        this.hinsAmount = hinsAmount;
    }

    public String getDesiKg() {
        return desiKg;
    }

    public void setDesiKg(String desiKg) {
        this.desiKg = desiKg;
    }

    public String getParcelCount() {
        return parcelCount;
    }

    public void setParcelCount(String parcelCount) {
        this.parcelCount = parcelCount;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
}
