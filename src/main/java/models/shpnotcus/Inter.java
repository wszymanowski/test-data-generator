package models.shpnotcus;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class Inter {

    private String NUMORDER;
    private String PARCELTYPE;
    private String CAMOUNT;
    private String CURRENCY;
    private String CAMOUNTEX;
    private String CURRENCYEX;
    private String CTERMS;
    private String CPAPER;
    private String CCOMMENT;
    private String CLEARANCECLEARED;
    private String HIGHLOWVALUE;
    private String PREALERTSTATUS;
    private String CINVOICE;
    private String CINVOICEDATE;
    private String CNAME1;
    private String CNAME2;
    private String CSTREET;
    private String CPROPNUM;
    private String CCOUNTRYCODE;
    private String CSTATE;
    private String CZIPCODE;
    private String CTOWN;
    private String CGPSLAT;
    private String CGPSLONG;
    private String CCONTACT;
    private String CPHONE;
    private String CFAX;
    private String CEMAIL;
    private String CGLN;
    private String CVATNO;
    private String CNUMBER;
    private String SHIPMRN;
    private String CEORI;
    private String SINAME1;
    private String SINAME2;
    private String SISTREET;
    private String SIPROPNUM;
    private String SICOUNTRYCODE;
    private String SISTATE;
    private String SIZIPCODE;
    private String SITOWN;
    private String SIGPSLAT;
    private String SIGPSLONG;
    private String SICONTACT;
    private String SIPHONE;
    private String OPCODE;
    private String NUMBEROFARTICLE;
    private String DESTCOUNTRYREG;

    public String getNUMORDER() {
        return NUMORDER;
    }

    public void setNUMORDER(String NUMORDER) {
        this.NUMORDER = NUMORDER;
    }

    public String getPARCELTYPE() {
        return PARCELTYPE;
    }

    public void setPARCELTYPE(String PARCELTYPE) {
        this.PARCELTYPE = PARCELTYPE;
    }

    public String getCAMOUNT() {
        return CAMOUNT;
    }

    public void setCAMOUNT(String CAMOUNT) {
        this.CAMOUNT = CAMOUNT;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    public String getCAMOUNTEX() {
        return CAMOUNTEX;
    }

    public void setCAMOUNTEX(String CAMOUNTEX) {
        this.CAMOUNTEX = CAMOUNTEX;
    }

    public String getCURRENCYEX() {
        return CURRENCYEX;
    }

    public void setCURRENCYEX(String CURRENCYEX) {
        this.CURRENCYEX = CURRENCYEX;
    }

    public String getCTERMS() {
        return CTERMS;
    }

    public void setCTERMS(String CTERMS) {
        this.CTERMS = CTERMS;
    }

    public String getCPAPER() {
        return CPAPER;
    }

    public void setCPAPER(String CPAPER) {
        this.CPAPER = CPAPER;
    }

    public String getCCOMMENT() {
        return CCOMMENT;
    }

    public void setCCOMMENT(String CCOMMENT) {
        this.CCOMMENT = CCOMMENT;
    }

    public String getCLEARANCECLEARED() {
        return CLEARANCECLEARED;
    }

    public void setCLEARANCECLEARED(String CLEARANCECLEARED) {
        this.CLEARANCECLEARED = CLEARANCECLEARED;
    }

    public String getHIGHLOWVALUE() {
        return HIGHLOWVALUE;
    }

    public void setHIGHLOWVALUE(String HIGHLOWVALUE) {
        this.HIGHLOWVALUE = HIGHLOWVALUE;
    }

    public String getPREALERTSTATUS() {
        return PREALERTSTATUS;
    }

    public void setPREALERTSTATUS(String PREALERTSTATUS) {
        this.PREALERTSTATUS = PREALERTSTATUS;
    }

    public String getCINVOICE() {
        return CINVOICE;
    }

    public void setCINVOICE(String CINVOICE) {
        this.CINVOICE = CINVOICE;
    }

    public String getCINVOICEDATE() {
        return CINVOICEDATE;
    }

    public void setCINVOICEDATE(String CINVOICEDATE) {
        this.CINVOICEDATE = CINVOICEDATE;
    }

    public String getCNAME1() {
        return CNAME1;
    }

    public void setCNAME1(String CNAME1) {
        this.CNAME1 = CNAME1;
    }

    public String getCNAME2() {
        return CNAME2;
    }

    public void setCNAME2(String CNAME2) {
        this.CNAME2 = CNAME2;
    }

    public String getCSTREET() {
        return CSTREET;
    }

    public void setCSTREET(String CSTREET) {
        this.CSTREET = CSTREET;
    }

    public String getCPROPNUM() {
        return CPROPNUM;
    }

    public void setCPROPNUM(String CPROPNUM) {
        this.CPROPNUM = CPROPNUM;
    }

    public String getCCOUNTRYCODE() {
        return CCOUNTRYCODE;
    }

    public void setCCOUNTRYCODE(String CCOUNTRYCODE) {
        this.CCOUNTRYCODE = CCOUNTRYCODE;
    }

    public String getCSTATE() {
        return CSTATE;
    }

    public void setCSTATE(String CSTATE) {
        this.CSTATE = CSTATE;
    }

    public String getCZIPCODE() {
        return CZIPCODE;
    }

    public void setCZIPCODE(String CZIPCODE) {
        this.CZIPCODE = CZIPCODE;
    }

    public String getCTOWN() {
        return CTOWN;
    }

    public void setCTOWN(String CTOWN) {
        this.CTOWN = CTOWN;
    }

    public String getCGPSLAT() {
        return CGPSLAT;
    }

    public void setCGPSLAT(String CGPSLAT) {
        this.CGPSLAT = CGPSLAT;
    }

    public String getCGPSLONG() {
        return CGPSLONG;
    }

    public void setCGPSLONG(String CGPSLONG) {
        this.CGPSLONG = CGPSLONG;
    }

    public String getCCONTACT() {
        return CCONTACT;
    }

    public void setCCONTACT(String CCONTACT) {
        this.CCONTACT = CCONTACT;
    }

    public String getCPHONE() {
        return CPHONE;
    }

    public void setCPHONE(String CPHONE) {
        this.CPHONE = CPHONE;
    }

    public String getCFAX() {
        return CFAX;
    }

    public void setCFAX(String CFAX) {
        this.CFAX = CFAX;
    }

    public String getCEMAIL() {
        return CEMAIL;
    }

    public void setCEMAIL(String CEMAIL) {
        this.CEMAIL = CEMAIL;
    }

    public String getCGLN() {
        return CGLN;
    }

    public void setCGLN(String CGLN) {
        this.CGLN = CGLN;
    }

    public String getCVATNO() {
        return CVATNO;
    }

    public void setCVATNO(String CVATNO) {
        this.CVATNO = CVATNO;
    }

    public String getCNUMBER() {
        return CNUMBER;
    }

    public void setCNUMBER(String CNUMBER) {
        this.CNUMBER = CNUMBER;
    }

    public String getSHIPMRN() {
        return SHIPMRN;
    }

    public void setSHIPMRN(String SHIPMRN) {
        this.SHIPMRN = SHIPMRN;
    }

    public String getCEORI() {
        return CEORI;
    }

    public void setCEORI(String CEORI) {
        this.CEORI = CEORI;
    }

    public String getSINAME1() {
        return SINAME1;
    }

    public void setSINAME1(String SINAME1) {
        this.SINAME1 = SINAME1;
    }

    public String getSINAME2() {
        return SINAME2;
    }

    public void setSINAME2(String SINAME2) {
        this.SINAME2 = SINAME2;
    }

    public String getSISTREET() {
        return SISTREET;
    }

    public void setSISTREET(String SISTREET) {
        this.SISTREET = SISTREET;
    }

    public String getSIPROPNUM() {
        return SIPROPNUM;
    }

    public void setSIPROPNUM(String SIPROPNUM) {
        this.SIPROPNUM = SIPROPNUM;
    }

    public String getSICOUNTRYCODE() {
        return SICOUNTRYCODE;
    }

    public void setSICOUNTRYCODE(String SICOUNTRYCODE) {
        this.SICOUNTRYCODE = SICOUNTRYCODE;
    }

    public String getSISTATE() {
        return SISTATE;
    }

    public void setSISTATE(String SISTATE) {
        this.SISTATE = SISTATE;
    }

    public String getSIZIPCODE() {
        return SIZIPCODE;
    }

    public void setSIZIPCODE(String SIZIPCODE) {
        this.SIZIPCODE = SIZIPCODE;
    }

    public String getSITOWN() {
        return SITOWN;
    }

    public void setSITOWN(String SITOWN) {
        this.SITOWN = SITOWN;
    }

    public String getSIGPSLAT() {
        return SIGPSLAT;
    }

    public void setSIGPSLAT(String SIGPSLAT) {
        this.SIGPSLAT = SIGPSLAT;
    }

    public String getSIGPSLONG() {
        return SIGPSLONG;
    }

    public void setSIGPSLONG(String SIGPSLONG) {
        this.SIGPSLONG = SIGPSLONG;
    }

    public String getSICONTACT() {
        return SICONTACT;
    }

    public void setSICONTACT(String SICONTACT) {
        this.SICONTACT = SICONTACT;
    }

    public String getSIPHONE() {
        return SIPHONE;
    }

    public void setSIPHONE(String SIPHONE) {
        this.SIPHONE = SIPHONE;
    }

    public String getOPCODE() {
        return OPCODE;
    }

    public void setOPCODE(String OPCODE) {
        this.OPCODE = OPCODE;
    }

    public String getNUMBEROFARTICLE() {
        return NUMBEROFARTICLE;
    }

    public void setNUMBEROFARTICLE(String NUMBEROFARTICLE) {
        this.NUMBEROFARTICLE = NUMBEROFARTICLE;
    }

    public String getDESTCOUNTRYREG() {
        return DESTCOUNTRYREG;
    }

    public void setDESTCOUNTRYREG(String DESTCOUNTRYREG) {
        this.DESTCOUNTRYREG = DESTCOUNTRYREG;
    }
}
