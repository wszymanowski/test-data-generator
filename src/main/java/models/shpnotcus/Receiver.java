package models.shpnotcus;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class Receiver {

    private String NUMORDER;
    private String RCUSTID;
    private String RNAME1;
    private String RNAME2;
    private String RCOMPNAME;
    private String RCOMPNAME2;
    private String RPUDOID;
    private String RSTREET;
    private String RPROPNUM;
    private String RADD2;
    private String RADD3;
    private String RFLOOR;
    private String RBUILDING;
    private String RDEPARTMENT;
    private String RCOUNTRYCODE;
    private String RSTATE;
    private String RZIPCODE;
    private String RTOWN;
    private String RDOORCODE;
    private String RCONTACT;
    private String RCONTACTPHO1;
    private String RCONTACTPHO2;
    private String RINTERPHONENAME;
    private String RFAX;
    private String REMAIL;
    private String RCOMMENT;
    private String RGPSLAT;
    private String RGPSLONG;


    public String getNUMORDER() {
        return NUMORDER;
    }

    public void setNUMORDER(String NUMORDER) {
        this.NUMORDER = NUMORDER;
    }

    public String getRCUSTID() {
        return RCUSTID;
    }

    public void setRCUSTID(String RCUSTID) {
        this.RCUSTID = RCUSTID;
    }

    public String getRNAME1() {
        return RNAME1;
    }

    public void setRNAME1(String RNAME1) {
        this.RNAME1 = RNAME1;
    }

    public String getRNAME2() {
        return RNAME2;
    }

    public void setRNAME2(String RNAME2) {
        this.RNAME2 = RNAME2;
    }

    public String getRCOMPNAME() {
        return RCOMPNAME;
    }

    public void setRCOMPNAME(String RCOMPNAME) {
        this.RCOMPNAME = RCOMPNAME;
    }

    public String getRCOMPNAME2() {
        return RCOMPNAME2;
    }

    public void setRCOMPNAME2(String RCOMPNAME2) {
        this.RCOMPNAME2 = RCOMPNAME2;
    }

    public String getRPUDOID() {
        return RPUDOID;
    }

    public void setRPUDOID(String RPUDOID) {
        this.RPUDOID = RPUDOID;
    }

    public String getRSTREET() {
        return RSTREET;
    }

    public void setRSTREET(String RSTREET) {
        this.RSTREET = RSTREET;
    }

    public String getRPROPNUM() {
        return RPROPNUM;
    }

    public void setRPROPNUM(String RPROPNUM) {
        this.RPROPNUM = RPROPNUM;
    }

    public String getRADD2() {
        return RADD2;
    }

    public void setRADD2(String RADD2) {
        this.RADD2 = RADD2;
    }

    public String getRADD3() {
        return RADD3;
    }

    public void setRADD3(String RADD3) {
        this.RADD3 = RADD3;
    }

    public String getRFLOOR() {
        return RFLOOR;
    }

    public void setRFLOOR(String RFLOOR) {
        this.RFLOOR = RFLOOR;
    }

    public String getRBUILDING() {
        return RBUILDING;
    }

    public void setRBUILDING(String RBUILDING) {
        this.RBUILDING = RBUILDING;
    }

    public String getRDEPARTMENT() {
        return RDEPARTMENT;
    }

    public void setRDEPARTMENT(String RDEPARTMENT) {
        this.RDEPARTMENT = RDEPARTMENT;
    }

    public String getRCOUNTRYCODE() {
        return RCOUNTRYCODE;
    }

    public void setRCOUNTRYCODE(String RCOUNTRYCODE) {
        this.RCOUNTRYCODE = RCOUNTRYCODE;
    }

    public String getRSTATE() {
        return RSTATE;
    }

    public void setRSTATE(String RSTATE) {
        this.RSTATE = RSTATE;
    }

    public String getRZIPCODE() {
        return RZIPCODE;
    }

    public void setRZIPCODE(String RZIPCODE) {
        this.RZIPCODE = RZIPCODE;
    }

    public String getRTOWN() {
        return RTOWN;
    }

    public void setRTOWN(String RTOWN) {
        this.RTOWN = RTOWN;
    }

    public String getRDOORCODE() {
        return RDOORCODE;
    }

    public void setRDOORCODE(String RDOORCODE) {
        this.RDOORCODE = RDOORCODE;
    }

    public String getRCONTACT() {
        return RCONTACT;
    }

    public void setRCONTACT(String RCONTACT) {
        this.RCONTACT = RCONTACT;
    }

    public String getRCONTACTPHO1() {
        return RCONTACTPHO1;
    }

    public void setRCONTACTPHO1(String RCONTACTPHO1) {
        this.RCONTACTPHO1 = RCONTACTPHO1;
    }

    public String getRCONTACTPHO2() {
        return RCONTACTPHO2;
    }

    public void setRCONTACTPHO2(String RCONTACTPHO2) {
        this.RCONTACTPHO2 = RCONTACTPHO2;
    }

    public String getRINTERPHONENAME() {
        return RINTERPHONENAME;
    }

    public void setRINTERPHONENAME(String RINTERPHONENAME) {
        this.RINTERPHONENAME = RINTERPHONENAME;
    }

    public String getRFAX() {
        return RFAX;
    }

    public void setRFAX(String RFAX) {
        this.RFAX = RFAX;
    }

    public String getREMAIL() {
        return REMAIL;
    }

    public void setREMAIL(String REMAIL) {
        this.REMAIL = REMAIL;
    }

    public String getRCOMMENT() {
        return RCOMMENT;
    }

    public void setRCOMMENT(String RCOMMENT) {
        this.RCOMMENT = RCOMMENT;
    }

    public String getRGPSLAT() {
        return RGPSLAT;
    }

    public void setRGPSLAT(String RGPSLAT) {
        this.RGPSLAT = RGPSLAT;
    }

    public String getRGPSLONG() {
        return RGPSLONG;
    }

    public void setRGPSLONG(String RGPSLONG) {
        this.RGPSLONG = RGPSLONG;
    }
}
