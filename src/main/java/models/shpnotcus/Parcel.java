package models.shpnotcus;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class Parcel {


    private String NUMORDER;
    private String PARCELNUMBER;
    private String PARCELNUMBERCCKEY;
    private String PARCELRANK;
    private String SENDPARCELREF1;
    private String SENDPARCELREF2;
    private String SENDPARCELREF3;
    private String SENDPARCELREF4;
    private String RECPARCELREF;
    private String SERVICECODE;
    private String PPARTNERREF1;
    private String PPARTNERREF2;
    private String PPARTNERREF3;
    private String PPARTNERREF4;
    private String DIMENSION;
    private String DECLAREDWEIGHT;
    private String MEASUREDWEIGHT;
    private String HINSAMOUNT;
    private String HINSCURRENCY;
    private String HINSCONTENT;
    private String HAZLQ;
    private String HZDPACKCODE;
    private String OPCODE;
    private String PCONTENT;
    private String ORIGINPARCELNUMBER;
    private String POWNERBU;
    private String PPARTNERCODE;
    private String ASCODE;
    private String BAGNO;


    public String getNUMORDER() {
        return NUMORDER;
    }

    public void setNUMORDER(String NUMORDER) {
        this.NUMORDER = NUMORDER;
    }

    public String getPARCELNUMBER() {
        return PARCELNUMBER;
    }

    public void setPARCELNUMBER(String PARCELNUMBER) {
        this.PARCELNUMBER = PARCELNUMBER;
    }

    public String getPARCELNUMBERCCKEY() {
        return PARCELNUMBERCCKEY;
    }

    public void setPARCELNUMBERCCKEY(String PARCELNUMBERCCKEY) {
        this.PARCELNUMBERCCKEY = PARCELNUMBERCCKEY;
    }

    public String getPARCELRANK() {
        return PARCELRANK;
    }

    public void setPARCELRANK(String PARCELRANK) {
        this.PARCELRANK = PARCELRANK;
    }

    public String getSENDPARCELREF1() {
        return SENDPARCELREF1;
    }

    public void setSENDPARCELREF1(String SENDPARCELREF1) {
        this.SENDPARCELREF1 = SENDPARCELREF1;
    }

    public String getSENDPARCELREF2() {
        return SENDPARCELREF2;
    }

    public void setSENDPARCELREF2(String SENDPARCELREF2) {
        this.SENDPARCELREF2 = SENDPARCELREF2;
    }

    public String getSENDPARCELREF3() {
        return SENDPARCELREF3;
    }

    public void setSENDPARCELREF3(String SENDPARCELREF3) {
        this.SENDPARCELREF3 = SENDPARCELREF3;
    }

    public String getSENDPARCELREF4() {
        return SENDPARCELREF4;
    }

    public void setSENDPARCELREF4(String SENDPARCELREF4) {
        this.SENDPARCELREF4 = SENDPARCELREF4;
    }

    public String getRECPARCELREF() {
        return RECPARCELREF;
    }

    public void setRECPARCELREF(String RECPARCELREF) {
        this.RECPARCELREF = RECPARCELREF;
    }

    public String getSERVICECODE() {
        return SERVICECODE;
    }

    public void setSERVICECODE(String SERVICECODE) {
        this.SERVICECODE = SERVICECODE;
    }

    public String getPPARTNERREF1() {
        return PPARTNERREF1;
    }

    public void setPPARTNERREF1(String PPARTNERREF1) {
        this.PPARTNERREF1 = PPARTNERREF1;
    }

    public String getPPARTNERREF2() {
        return PPARTNERREF2;
    }

    public void setPPARTNERREF2(String PPARTNERREF2) {
        this.PPARTNERREF2 = PPARTNERREF2;
    }

    public String getPPARTNERREF3() {
        return PPARTNERREF3;
    }

    public void setPPARTNERREF3(String PPARTNERREF3) {
        this.PPARTNERREF3 = PPARTNERREF3;
    }

    public String getPPARTNERREF4() {
        return PPARTNERREF4;
    }

    public void setPPARTNERREF4(String PPARTNERREF4) {
        this.PPARTNERREF4 = PPARTNERREF4;
    }

    public String getDIMENSION() {
        return DIMENSION;
    }

    public void setDIMENSION(String DIMENSION) {
        this.DIMENSION = DIMENSION;
    }

    public String getDECLAREDWEIGHT() {
        return DECLAREDWEIGHT;
    }

    public void setDECLAREDWEIGHT(String DECLAREDWEIGHT) {
        this.DECLAREDWEIGHT = DECLAREDWEIGHT;
    }

    public String getMEASUREDWEIGHT() {
        return MEASUREDWEIGHT;
    }

    public void setMEASUREDWEIGHT(String MEASUREDWEIGHT) {
        this.MEASUREDWEIGHT = MEASUREDWEIGHT;
    }

    public String getHINSAMOUNT() {
        return HINSAMOUNT;
    }

    public void setHINSAMOUNT(String HINSAMOUNT) {
        this.HINSAMOUNT = HINSAMOUNT;
    }

    public String getHINSCURRENCY() {
        return HINSCURRENCY;
    }

    public void setHINSCURRENCY(String HINSCURRENCY) {
        this.HINSCURRENCY = HINSCURRENCY;
    }

    public String getHINSCONTENT() {
        return HINSCONTENT;
    }

    public void setHINSCONTENT(String HINSCONTENT) {
        this.HINSCONTENT = HINSCONTENT;
    }

    public String getHAZLQ() {
        return HAZLQ;
    }

    public void setHAZLQ(String HAZLQ) {
        this.HAZLQ = HAZLQ;
    }

    public String getHZDPACKCODE() {
        return HZDPACKCODE;
    }

    public void setHZDPACKCODE(String HZDPACKCODE) {
        this.HZDPACKCODE = HZDPACKCODE;
    }

    public String getOPCODE() {
        return OPCODE;
    }

    public void setOPCODE(String OPCODE) {
        this.OPCODE = OPCODE;
    }

    public String getPCONTENT() {
        return PCONTENT;
    }

    public void setPCONTENT(String PCONTENT) {
        this.PCONTENT = PCONTENT;
    }

    public String getORIGINPARCELNUMBER() {
        return ORIGINPARCELNUMBER;
    }

    public void setORIGINPARCELNUMBER(String ORIGINPARCELNUMBER) {
        this.ORIGINPARCELNUMBER = ORIGINPARCELNUMBER;
    }

    public String getPOWNERBU() {
        return POWNERBU;
    }

    public void setPOWNERBU(String POWNERBU) {
        this.POWNERBU = POWNERBU;
    }

    public String getPPARTNERCODE() {
        return PPARTNERCODE;
    }

    public void setPPARTNERCODE(String PPARTNERCODE) {
        this.PPARTNERCODE = PPARTNERCODE;
    }

    public String getASCODE() {
        return ASCODE;
    }

    public void setASCODE(String ASCODE) {
        this.ASCODE = ASCODE;
    }

    public String getBAGNO() {
        return BAGNO;
    }

    public void setBAGNO(String BAGNO) {
        this.BAGNO = BAGNO;
    }
}
