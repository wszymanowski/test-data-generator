package models.shpnotcus;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class Shipment {

    private String NUMORDER;
    private String MPSID;
    private String CUSTOMSREF;
    private String MPSIDCCKEY;
    private String MPSCOMP;
    private String MPSCREF1;
    private String MPSCREF2;
    private String MPSCREF3;
    private String MPSCREF4;
    private String MPSBILLREF;
    private String MPSCOUNT;
    private String MPSVOLUME;
    private String MPSWEIGHT;
    private String SDEPOT;
    private String CDATE;
    private String CTIME;
    private String HARDWARE;
    private String RDEPOT;
    private String DSORT;
    private String SPTDATE;
    private String SPTTIME;
    private String SPTREALDATE;
    private String SPTREALTIME;
    private String DELMODALLOW;
    private String ROUTINGPLANVERSION;
    private String SPARTNERREF1;
    private String SPARTNERREF2;
    private String SPARTNERCODE;
    private String OSORT;
    private String SSORT;
    private String MSORT;
    private String COLREQUESTFLAG;
    private String ROUTINGPLACE;


    public String getNUMORDER() {
        return NUMORDER;
    }

    public void setNUMORDER(String NUMORDER) {
        this.NUMORDER = NUMORDER;
    }

    public String getMPSID() {
        return MPSID;
    }

    public void setMPSID(String MPSID) {
        this.MPSID = MPSID;
    }

    public String getCUSTOMSREF() {
        return CUSTOMSREF;
    }

    public void setCUSTOMSREF(String CUSTOMSREF) {
        this.CUSTOMSREF = CUSTOMSREF;
    }

    public String getMPSIDCCKEY() {
        return MPSIDCCKEY;
    }

    public void setMPSIDCCKEY(String MPSIDCCKEY) {
        this.MPSIDCCKEY = MPSIDCCKEY;
    }

    public String getMPSCOMP() {
        return MPSCOMP;
    }

    public void setMPSCOMP(String MPSCOMP) {
        this.MPSCOMP = MPSCOMP;
    }

    public String getMPSCREF1() {
        return MPSCREF1;
    }

    public void setMPSCREF1(String MPSCREF1) {
        this.MPSCREF1 = MPSCREF1;
    }

    public String getMPSCREF2() {
        return MPSCREF2;
    }

    public void setMPSCREF2(String MPSCREF2) {
        this.MPSCREF2 = MPSCREF2;
    }

    public String getMPSCREF3() {
        return MPSCREF3;
    }

    public void setMPSCREF3(String MPSCREF3) {
        this.MPSCREF3 = MPSCREF3;
    }

    public String getMPSCREF4() {
        return MPSCREF4;
    }

    public void setMPSCREF4(String MPSCREF4) {
        this.MPSCREF4 = MPSCREF4;
    }

    public String getMPSBILLREF() {
        return MPSBILLREF;
    }

    public void setMPSBILLREF(String MPSBILLREF) {
        this.MPSBILLREF = MPSBILLREF;
    }

    public String getMPSCOUNT() {
        return MPSCOUNT;
    }

    public void setMPSCOUNT(String MPSCOUNT) {
        this.MPSCOUNT = MPSCOUNT;
    }

    public String getMPSVOLUME() {
        return MPSVOLUME;
    }

    public void setMPSVOLUME(String MPSVOLUME) {
        this.MPSVOLUME = MPSVOLUME;
    }

    public String getMPSWEIGHT() {
        return MPSWEIGHT;
    }

    public void setMPSWEIGHT(String MPSWEIGHT) {
        this.MPSWEIGHT = MPSWEIGHT;
    }

    public String getSDEPOT() {
        return SDEPOT;
    }

    public void setSDEPOT(String SDEPOT) {
        this.SDEPOT = SDEPOT;
    }

    public String getCDATE() {
        return CDATE;
    }

    public void setCDATE(String CDATE) {
        this.CDATE = CDATE;
    }

    public String getCTIME() {
        return CTIME;
    }

    public void setCTIME(String CTIME) {
        this.CTIME = CTIME;
    }

    public String getHARDWARE() {
        return HARDWARE;
    }

    public void setHARDWARE(String HARDWARE) {
        this.HARDWARE = HARDWARE;
    }

    public String getRDEPOT() {
        return RDEPOT;
    }

    public void setRDEPOT(String RDEPOT) {
        this.RDEPOT = RDEPOT;
    }

    public String getDSORT() {
        return DSORT;
    }

    public void setDSORT(String DSORT) {
        this.DSORT = DSORT;
    }

    public String getSPTDATE() {
        return SPTDATE;
    }

    public void setSPTDATE(String SPTDATE) {
        this.SPTDATE = SPTDATE;
    }

    public String getSPTTIME() {
        return SPTTIME;
    }

    public void setSPTTIME(String SPTTIME) {
        this.SPTTIME = SPTTIME;
    }

    public String getSPTREALDATE() {
        return SPTREALDATE;
    }

    public void setSPTREALDATE(String SPTREALDATE) {
        this.SPTREALDATE = SPTREALDATE;
    }

    public String getSPTREALTIME() {
        return SPTREALTIME;
    }

    public void setSPTREALTIME(String SPTREALTIME) {
        this.SPTREALTIME = SPTREALTIME;
    }

    public String getDELMODALLOW() {
        return DELMODALLOW;
    }

    public void setDELMODALLOW(String DELMODALLOW) {
        this.DELMODALLOW = DELMODALLOW;
    }

    public String getROUTINGPLANVERSION() {
        return ROUTINGPLANVERSION;
    }

    public void setROUTINGPLANVERSION(String ROUTINGPLANVERSION) {
        this.ROUTINGPLANVERSION = ROUTINGPLANVERSION;
    }

    public String getSPARTNERREF1() {
        return SPARTNERREF1;
    }

    public void setSPARTNERREF1(String SPARTNERREF1) {
        this.SPARTNERREF1 = SPARTNERREF1;
    }

    public String getSPARTNERREF2() {
        return SPARTNERREF2;
    }

    public void setSPARTNERREF2(String SPARTNERREF2) {
        this.SPARTNERREF2 = SPARTNERREF2;
    }

    public String getSPARTNERCODE() {
        return SPARTNERCODE;
    }

    public void setSPARTNERCODE(String SPARTNERCODE) {
        this.SPARTNERCODE = SPARTNERCODE;
    }

    public String getOSORT() {
        return OSORT;
    }

    public void setOSORT(String OSORT) {
        this.OSORT = OSORT;
    }

    public String getSSORT() {
        return SSORT;
    }

    public void setSSORT(String SSORT) {
        this.SSORT = SSORT;
    }

    public String getMSORT() {
        return MSORT;
    }

    public void setMSORT(String MSORT) {
        this.MSORT = MSORT;
    }

    public String getCOLREQUESTFLAG() {
        return COLREQUESTFLAG;
    }

    public void setCOLREQUESTFLAG(String COLREQUESTFLAG) {
        this.COLREQUESTFLAG = COLREQUESTFLAG;
    }

    public String getROUTINGPLACE() {
        return ROUTINGPLACE;
    }

    public void setROUTINGPLACE(String ROUTINGPLACE) {
        this.ROUTINGPLACE = ROUTINGPLACE;
    }
}
