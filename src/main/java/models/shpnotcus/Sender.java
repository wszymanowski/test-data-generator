package models.shpnotcus;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class Sender {

    private String NUMORDER;
    private String SCUSTACCNUMBER;
    private String SCUSTSUBACCNUMBER;
    private String SCOMPNAME;
    private String SUNIQCUSTID;
    private String SNAME1;
    private String SNAME2;
    private String SSTREET;
    private String SPROPNUM;
    private String SADD2;
    private String SADD3;
    private String SFLOOR;
    private String SBUILDING;
    private String SDEPARTMENT;
    private String SCOUNTRYCODE;
    private String SSTATE;
    private String SZIPCODE;
    private String STOWN;
    private String SCONTACT;
    private String SPHONE;
    private String SFAX;
    private String SEMAIL;
    private String SCOMMENT;
    private String SGLN;
    private String SGPSLAT;
    private String SGPSLONG;
    private String SACCOUNTOWNER;

    public String getNUMORDER() {
        return NUMORDER;
    }

    public void setNUMORDER(String NUMORDER) {
        this.NUMORDER = NUMORDER;
    }

    public String getSCUSTACCNUMBER() {
        return SCUSTACCNUMBER;
    }

    public void setSCUSTACCNUMBER(String SCUSTACCNUMBER) {
        this.SCUSTACCNUMBER = SCUSTACCNUMBER;
    }

    public String getSCUSTSUBACCNUMBER() {
        return SCUSTSUBACCNUMBER;
    }

    public void setSCUSTSUBACCNUMBER(String SCUSTSUBACCNUMBER) {
        this.SCUSTSUBACCNUMBER = SCUSTSUBACCNUMBER;
    }

    public String getSCOMPNAME() {
        return SCOMPNAME;
    }

    public void setSCOMPNAME(String SCOMPNAME) {
        this.SCOMPNAME = SCOMPNAME;
    }

    public String getSUNIQCUSTID() {
        return SUNIQCUSTID;
    }

    public void setSUNIQCUSTID(String SUNIQCUSTID) {
        this.SUNIQCUSTID = SUNIQCUSTID;
    }

    public String getSNAME1() {
        return SNAME1;
    }

    public void setSNAME1(String SNAME1) {
        this.SNAME1 = SNAME1;
    }

    public String getSNAME2() {
        return SNAME2;
    }

    public void setSNAME2(String SNAME2) {
        this.SNAME2 = SNAME2;
    }

    public String getSSTREET() {
        return SSTREET;
    }

    public void setSSTREET(String SSTREET) {
        this.SSTREET = SSTREET;
    }

    public String getSPROPNUM() {
        return SPROPNUM;
    }

    public void setSPROPNUM(String SPROPNUM) {
        this.SPROPNUM = SPROPNUM;
    }

    public String getSADD2() {
        return SADD2;
    }

    public void setSADD2(String SADD2) {
        this.SADD2 = SADD2;
    }

    public String getSADD3() {
        return SADD3;
    }

    public void setSADD3(String SADD3) {
        this.SADD3 = SADD3;
    }

    public String getSFLOOR() {
        return SFLOOR;
    }

    public void setSFLOOR(String SFLOOR) {
        this.SFLOOR = SFLOOR;
    }

    public String getSBUILDING() {
        return SBUILDING;
    }

    public void setSBUILDING(String SBUILDING) {
        this.SBUILDING = SBUILDING;
    }

    public String getSDEPARTMENT() {
        return SDEPARTMENT;
    }

    public void setSDEPARTMENT(String SDEPARTMENT) {
        this.SDEPARTMENT = SDEPARTMENT;
    }

    public String getSCOUNTRYCODE() {
        return SCOUNTRYCODE;
    }

    public void setSCOUNTRYCODE(String SCOUNTRYCODE) {
        this.SCOUNTRYCODE = SCOUNTRYCODE;
    }

    public String getSSTATE() {
        return SSTATE;
    }

    public void setSSTATE(String SSTATE) {
        this.SSTATE = SSTATE;
    }

    public String getSZIPCODE() {
        return SZIPCODE;
    }

    public void setSZIPCODE(String SZIPCODE) {
        this.SZIPCODE = SZIPCODE;
    }

    public String getSTOWN() {
        return STOWN;
    }

    public void setSTOWN(String STOWN) {
        this.STOWN = STOWN;
    }

    public String getSCONTACT() {
        return SCONTACT;
    }

    public void setSCONTACT(String SCONTACT) {
        this.SCONTACT = SCONTACT;
    }

    public String getSPHONE() {
        return SPHONE;
    }

    public void setSPHONE(String SPHONE) {
        this.SPHONE = SPHONE;
    }

    public String getSFAX() {
        return SFAX;
    }

    public void setSFAX(String SFAX) {
        this.SFAX = SFAX;
    }

    public String getSEMAIL() {
        return SEMAIL;
    }

    public void setSEMAIL(String SEMAIL) {
        this.SEMAIL = SEMAIL;
    }

    public String getSCOMMENT() {
        return SCOMMENT;
    }

    public void setSCOMMENT(String SCOMMENT) {
        this.SCOMMENT = SCOMMENT;
    }

    public String getSGLN() {
        return SGLN;
    }

    public void setSGLN(String SGLN) {
        this.SGLN = SGLN;
    }

    public String getSGPSLAT() {
        return SGPSLAT;
    }

    public void setSGPSLAT(String SGPSLAT) {
        this.SGPSLAT = SGPSLAT;
    }

    public String getSGPSLONG() {
        return SGPSLONG;
    }

    public void setSGPSLONG(String SGPSLONG) {
        this.SGPSLONG = SGPSLONG;
    }

    public String getSACCOUNTOWNER() {
        return SACCOUNTOWNER;
    }

    public void setSACCOUNTOWNER(String SACCOUNTOWNER) {
        this.SACCOUNTOWNER = SACCOUNTOWNER;
    }
}
