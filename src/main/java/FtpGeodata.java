import com.jcraft.jsch.*;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class FtpGeodata {

    public void sendFilesToFtpGeodata(){
                JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession("geoapp", "ftp-geodata.geopostgroup.com", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword("U2xmNi34");
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
//            sftpChannel.get("remotefile.txt", "localfile.txt");
            String localFile = "src/main/resources/GEODATA_SHPNOTCUS_FR06_GTK_D20200327T114023_101019";
            String remoteDir = "/sftp/000044/IN/SHPNOTCUS/";
            sftpChannel.put(localFile, remoteDir);
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }
}
