import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class Gtts {

    private Pattern regex = Pattern.compile("<cdParcel>(.+?)<\\/cdParcel>");
    private String found;

    public void gttsValidation(){



//        HttpResponse<JsonNode> response = null;
        HttpResponse<String> response = null;
        try{

//        Unirest.setTimeouts(0, 0);
        response = Unirest.post("https://test.dpsin.dpdgroup.com/gtts/soap/GttsGeoCCPService?Content-Type=text/xml;charset=UTF-8%20")
                .header("Content-Type", "application/xml")
                .body("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:api=\"http://api.gtts.geopostgroup.com/\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <api:getMyParcel>\r\n         <input>\r\n            <credentials>\r\n               <wsUser>gtts</wsUser>\r\n               <wsPassword>GTTSPassw123</wsPassword>\r\n            </credentials>\r\n            <lang>EN</lang>\r\n            <inputIds>0575P2144581002</inputIds>\r\n         </input>\r\n      </api:getMyParcel>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>")
                .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        System.out.println(response.getBody());

        Matcher matcher = regex.matcher(response.getBody());
        if (matcher.find()) found = matcher.group(0);
        System.out.println(found);
        //until found null -> repeat + time limitation + rest assured
    }
}
