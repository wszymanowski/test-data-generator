import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class FtpDPDRegister {

    File firstLocalFile = new File("src/main/resources/generated_parcels/SCANDATA.03.PG0010201900093657.20200329.csv");

    public void sendFilesToFtp() {
        String server = "10.55.24.176";
        int port = 21;
        String user = "upload";
        String pass = "iU3d9Y";

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream
//            File firstLocalFile = new File("D:/Test/Projects.zip");

            String firstRemoteFile = "/home/upload/scandata/" + "SCANDATA.03.PG0010201900093657.20200329.csv";
            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }
        } catch (
                IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


}
