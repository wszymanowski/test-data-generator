package providers;

import models.shipment.Shipment;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class StatusdataFactory {

    String today = Date.getTodaysDate();
    String time = Date.getTime();
    String scanCode = "03"; // move to config
    String unitId = "0560"; // move to config or take from xml
    //    String depot = vars.get("unitIdTmp");
    String countryParam = "BE"; // move to Config
    String countryIsoCode = "428"; // iso map
    // Add eventless condition
//
//
    String statusdataFilename = "STATUSDATA_GEOTRACK_DE01_D" + today + "T" + time + "_101019";
    String statusdataSemFilename = "STATUSDATA_GEOTRACK_DE01_D" + today + "T" + time + "_101019.sem";
    //
    String statusdataHeader = "STATUSDATA:PARCELNO;SCAN_CODE;DEPOT_CODE;DEPOTNAME;EVENT_DATE_TIME;ROUTE;TOUR;PCODE;SERVICE;CONSIGNEE_COUNTRY_CODE;CONSIGNEE_ZIP;ADD_SERVICE_1;ADD_SERVICE_2;ADD_SERVICE_3;WEIGHT;CUSTOMER_REFERENCE;POD_IMAGE_REF;RECEIVER_NAME;INFO_TEXT;LOCATION;MEM_DATE_TIME;TRANSFER_FILENAME;TRANSFER_DATE_TIME;;";


    public LinkedHashMap<String, String> createStatusdata(Shipment shipment, ArrayList parcelNumbers) {
        LinkedHashMap<String, String> parcelNumbersAndScanCodes = new LinkedHashMap<>();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("src/main/resources/generated_parcels/" + statusdataSemFilename, "UTF-8");
            writer = new PrintWriter("src/main/resources/generated_parcels/" + statusdataFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(statusdataHeader);
        for (int i = 0; i < parcelNumbers.size(); i++) {
            scanCode = ScanCodes.getRandomScanCode();
            parcelNumbersAndScanCodes.put((String)parcelNumbers.get(i), scanCode);
            writer.print(parcelNumbers.get(i) + ";" + scanCode + ";" + unitId + ";Depot " + unitId + ";" + today + time + ";" + unitId + ";;;101;" + countryIsoCode + ";" + shipment.getOprShipmentVO().getReceiverZipCode() + ";;;;;;;;;;" + today + time + ";;;\n");
        }
        writer.close();
        return parcelNumbersAndScanCodes;
    }


}
