package providers;

import models.shipment.Shipment;
import models.shpnotcus.Receiver;
import models.shpnotcus.Sender;
import providers.shpnotcus.ReceiverFactory;
import providers.shpnotcus.SenderFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class ShpnotcusFactory {

    String today = Date.getTodaysDate();
    String time = Date.getTime();

    String unitId = "0560"; // move to config or take from xml
    String referenceNumber = ""; // move to config, try display on tmp for biz
    String contractNo = "PTDGTMP"; // move to config, no equivalent in xml
//    String contractNo = "Wiktor123"; // move to config, no equivalent in xml
    String senderCity = "Riga"; // move to config or take from xml
    String countryIsoCode = "428"; // iso map
    String emailAddress = "mail@com.pl"; //to remove anfter methods refactor



//    String depot = vars.get("unitIdTmp"); -> compare with unitId
//    String countryParam = vars.get("countryParameter"); -> take from xml move to config
    String shpnotcusFilename = "GEODATA_SHPNOTCUS_FR06_GTK_D" + today + "T" + time +"_101019";
    String shpnotcusSemFilename = "GEODATA_SHPNOTCUS_FR06_GTK_D" + today + "T" + time +"_101019.sem";
//
    String shpnotcusHeader = "#FILE;"+ shpnotcusFilename +";\n#ENCODING;UTF-8;\n#VERSION;03.00;\n#DEF;GEODATA:HEADER;VERSION;CLASSIFICATION;;\n#DEF;GEODATA:SHIPMENT;NUMORDER;MPSID;CUSTOMSREF;MPSIDCCKEY;MPSCOMP;MPSCREF1;MPSCREF2;MPSCREF3;MPSCREF4;MPSBILLREF;MPSCOUNT;MPSVOLUME;MPSWEIGHT;SDEPOT;CDATE;CTIME;HARDWARE;RDEPOT;DSORT;SPTDATE;SPTTIME;SPTREALDATE;SPTREALTIME;DELMODALLOW;ROUTINGPLANVERSION;SPARTNERREF1;SPARTNERREF2;SPARTNERCODE;OSORT;SSORT;MSORT;COLREQUESTFLAG;ROUTINGPLACE;;\n#DEF;GEODATA:SENDER;NUMORDER;SCUSTACCNUMBER;SCUSTSUBACCNUMBER;SCOMPNAME;SUNIQCUSTID;SNAME1;SNAME2;SSTREET;SPROPNUM;SADD2;SADD3;SFLOOR;SBUILDING;SDEPARTMENT;SCOUNTRYCODE;SSTATE;SZIPCODE;STOWN;SCONTACT;SPHONE;SFAX;SEMAIL;SCOMMENT;SGLN;SGPSLAT;SGPSLONG;SACCOUNTOWNER;;\n#DEF;GEODATA:RECEIVER;NUMORDER;RCUSTID;RNAME1;RNAME2;RCOMPNAME;RCOMPNAME2;RPUDOID;RSTREET;RPROPNUM;RADD2;RADD3;RFLOOR;RBUILDING;RDEPARTMENT;RCOUNTRYCODE;RSTATE;RZIPCODE;RTOWN;RDOORCODE;RCONTACT;RCONTACTPHO1;RCONTACTPHO2;RINTERPHONENAME;RFAX;REMAIL;RCOMMENT;RGPSLAT;RGPSLONG;;\n#DEF;GEODATA:INTER;NUMORDER;PARCELTYPE;CAMOUNT;CURRENCY;CAMOUNTEX;CURRENCYEX;CTERMS;CPAPER;CCOMMENT;CLEARANCECLEARED;HIGHLOWVALUE;PREALERTSTATUS;CINVOICE;CINVOICEDATE;CNAME1;CNAME2;CSTREET;CPROPNUM;CCOUNTRYCODE;CSTATE;CZIPCODE;CTOWN;CGPSLAT;CGPSLONG;CCONTACT;CPHONE;CFAX;CEMAIL;CGLN;CVATNO;CNUMBER;SHIPMRN;CEORI;SINAME1;SINAME2;SISTREET;SIPROPNUM;SICOUNTRYCODE;SISTATE;SIZIPCODE;SITOWN;SIGPSLAT;SIGPSLONG;SICONTACT;SIPHONE;OPCODE;NUMBEROFARTICLE;DESTCOUNTRYREG;;\n#DEF;GEODATA:INTERINVOICELINE;NUMORDER;CINVOICEPOSITION;QITEMS;CCONTENT;CTARIF;CAMOUNTLINE;CCOMMENT;CORIGIN;CNETWEIGHT;CGROSSWEIGHT;CPRODCODE;CRPRODTYPE;CFABRICCOMPOSITION;;\n#DEF;GEODATA:PERS;NUMORDER;PERSDELIVERY;PERSNAME;PERSPHONE1;PERSPHONE2;PERSID;PERSDEPOT;PERSNAME1;PERSNAME2;PERSCOMPNAME;PERSCOMPNAME2;PERSSTREET;PERSPROPNUM;PERSCOUNTRYCODE;PERSADD2;PERSADD3;PERSFLOOR;PERSBUILDING;PERSDEPARTMENT;PERSSTATE;PERSZIPCODE;PERSTOWN;PERSDOORCODE;PERSCONTACT;PERSINTERPHONENAME;PERSFAX;PERSEMAIL;PERSGLN;PERSCOMMENT;PERSGPSLAT;PERSGPSLONG;;\n#DEF;GEODATA:PARCEL;NUMORDER;PARCELNUMBER;PARCELNUMBERCCKEY;PARCELRANK;SENDPARCELREF1;SENDPARCELREF2;SENDPARCELREF3;SENDPARCELREF4;RECPARCELREF;SERVICECODE;PPARTNERREF1;PPARTNERREF2;PPARTNERREF3;PPARTNERREF4;DIMENSION;DECLAREDWEIGHT;MEASUREDWEIGHT;HINSAMOUNT;HINSCURRENCY;HINSCONTENT;HAZLQ;HZDPACKCODE;OPCODE;PCONTENT;ORIGINPARCELNUMBER;POWNERBU;PPARTNERCODE;ASCODE;BAGNO;;\n#DEF;GEODATA:COD;NUMORDER;NAMOUNT;NCURRENCY;NCOLLECTTYPE;;\n#DEF;GEODATA:MSG;NUMORDER;NOTIFSENDERCOMP;NOTIFSENDERCONTACT;MSGTYPE;MSGDESTTYPE;MSGDESTINATION;MSGTRIGGER;MSGLANG;MSGSENDERURL;;\nHEADER;03.00;SHPNOTCUS;";


    public ArrayList createShpnotcus(Shipment shipment,int numberOfIteration){
        PrintWriter writer = null;
        int loopConditionInt = 100 + numberOfIteration;
        ArrayList parcelNumbers = new ArrayList<>();
        try {
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusSemFilename, "UTF-8");
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(shpnotcusHeader);
        for (int i = 100; i<loopConditionInt; i++) {
            String parcelNumber = unitId + "P"+ time + i;
            parcelNumbers.add(parcelNumber);
            writer.print("SHIPMENT;2;"+ parcelNumber +";;A;0;"+referenceNumber+";;;;;1;;0;"+shipment.getOprShipmentVO().getDepartureUnitId()+";" + today + ";" + time +";S;"+shipment.getOprShipmentVO().getDeliveryUnitId()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getRouteDsort()+";" + today +";;;;0;;;;;;;;0;;\nSENDER;3;"+ contractNo+";;ATT_CUSTOMER1104;1234567778;;;"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getSenderStreet()+";1;;;;;"+senderCity+";"+countryIsoCode+";;"+shipment.getOprShipmentVO().getSenderZipCode()+";"+ senderCity+";;;;;Initial shipment = "+ parcelNumber +";;;;;\nRECEIVER;3;;Buchta, Adrian;;;;;"+shipment.getOprShipmentVO().getReceiverAddressText()+";10;THIS IS TEST, SHIPMENT IS FICTIVE;;;;"+shipment.getOprShipmentVO().getReceiverCity()+";"+countryIsoCode+";;"+shipment.getOprShipmentVO().getReceiverZipCode()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getReceiverTownName()+";;Adrian Buchta;+32132124124124;;;;"+emailAddress+";;;;\nINTER;3;;;;;;;;;F;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;INS;;;\nPARCEL;3;"+ parcelNumber +";2;;;;;;;"+shipment.getOprShipmentVO().getOprShpParcelVOArray().get(0).getServiceCode()+";;;;;;0;;0;;;0;;INS;;;;;;;\n");
        }
        writer.print("#END;");
        writer.close();
        return parcelNumbers;
    }

    public ArrayList createShpnotcusWithFakeData(Shipment shipment,int numberOfIteration){
        PrintWriter writer = null;
        int loopConditionInt = 100 + numberOfIteration;
        ArrayList parcelNumbers = new ArrayList<>();
        try {
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusSemFilename, "UTF-8");
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(shpnotcusHeader);
        for (int i = 100; i<loopConditionInt; i++) {
            String parcelNumber = unitId + "P"+ time + i;
            parcelNumbers.add(parcelNumber);
            Receiver receiver = new ReceiverFactory().getReceiver();
            Sender sender = new SenderFactory(contractNo).getSender();
            writer.print("SHIPMENT;2;"+ parcelNumber +";;A;0;"+referenceNumber+";;;;;1;;0;"+shipment.getOprShipmentVO().getDepartureUnitId()+";" + today + ";" + time +";S;"+shipment.getOprShipmentVO().getDeliveryUnitId()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getRouteDsort()+";" + today +";;;;0;;;;;;;;0;;\nSENDER;3;"+ sender.getSCUSTACCNUMBER()+";;"+ sender.getSCOMPNAME()+";"+sender.getSUNIQCUSTID()+";;;"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getSenderStreet()+";1;;;;;"+sender.getSTOWN()+";"+sender.getSCOUNTRYCODE()+";;"+shipment.getOprShipmentVO().getSenderZipCode()+";"+ sender.getSTOWN()+";;;;;Initial shipment = "+ parcelNumber +";;;;;\nRECEIVER;3;;"+receiver.getRNAME1()+";;;;;"+shipment.getOprShipmentVO().getReceiverAddressText()+";"+receiver.getRPROPNUM()+";"+receiver.getRADD2()+";;;;"+shipment.getOprShipmentVO().getReceiverCity()+";"+countryIsoCode+";;"+shipment.getOprShipmentVO().getReceiverZipCode()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getReceiverTownName()+";;Adrian Buchta;"+receiver.getRCONTACTPHO1()+";;;;"+receiver.getREMAIL()+";;;;\nINTER;3;;;;;;;;;F;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;INS;;;\nPARCEL;3;"+ parcelNumber +";2;;;;;;;"+shipment.getOprShipmentVO().getOprShpParcelVOArray().get(0).getServiceCode()+";;;;;;0;;0;;;0;;INS;;;;;;;\n");
        }
        writer.print("#END;");
        writer.close();
        return parcelNumbers;
    }

    public ArrayList createDefaultShpnotcus(Shipment shipment,int numberOfIteration){
        PrintWriter writer = null;
        int loopConditionInt = 100 + numberOfIteration;
        ArrayList parcelNumbers = new ArrayList<>();
        try {
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusSemFilename, "UTF-8");
            writer = new PrintWriter("src/main/resources/generated_parcels/"+shpnotcusFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(shpnotcusHeader);
        for (int i = 100; i<loopConditionInt; i++) {
            String parcelNumber = unitId + "P"+ time + i;
            parcelNumbers.add(parcelNumber);
            Receiver receiver = new ReceiverFactory().getReceiver();
            Sender sender = new SenderFactory(contractNo).getSender();
            writer.print("SHIPMENT;2;"+ parcelNumber +";;A;0;"+referenceNumber+";;;;;1;;0;"+shipment.getOprShipmentVO().getDepartureUnitId()+";" + today + ";" + time +";S;"+shipment.getOprShipmentVO().getDeliveryUnitId()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getRouteDsort()+";" + today +";;;;0;;;;;;;;0;;\n");
            writer.print("SENDER;3;"+ sender.getSCUSTACCNUMBER()+";;"+ sender.getSCOMPNAME()+";"+sender.getSUNIQCUSTID()+";;;"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getSenderStreet()+";1;;;;;"+sender.getSTOWN()+";"+sender.getSCOUNTRYCODE()+";;"+shipment.getOprShipmentVO().getSenderZipCode()+";"+ sender.getSTOWN()+";;;;;Initial shipment = "+ parcelNumber +";;;;;\n");
            writer.print("RECEIVER;3;;"+receiver.getRNAME1()+";;;;;"+shipment.getOprShipmentVO().getReceiverAddressText()+";"+receiver.getRPROPNUM()+";"+receiver.getRADD2()+";;;;"+shipment.getOprShipmentVO().getReceiverCity()+";"+countryIsoCode+";;"+shipment.getOprShipmentVO().getReceiverZipCode()+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getReceiverTownName()+";;Adrian Buchta;"+receiver.getRCONTACTPHO1()+";;;;"+receiver.getREMAIL()+";;;;\n");
            writer.print("INTER;3;;;;;;;;;F;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;INS;;;\n");
            writer.print("PARCEL;3;"+ parcelNumber +";2;;;;;;;"+shipment.getOprShipmentVO().getOprShpParcelVOArray().get(0).getServiceCode()+";;;;;;0;;0;;;0;;INS;;;;;;;\n");
        }
        writer.print("#END;");
        writer.close();
        return parcelNumbers;
    }




}
