package providers;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public abstract class ScanCodes {

    private static final HashMap<String, String> SCAN_CODES = new HashMap<String, String>() {
        {
            put("01", "01-Consolidation");
            put("02", "02-Inbound");
            put("03", "03-Out-for-delivery");
            put("04", "04-Drivers-return");
            put("05", "05-Pick-up");
            put("06", "06-System-return");
            put("07", "07-Outbound");
            put("08", "08-Warehouse");
            put("09", "09-Inbound-exception");
            put("10", "10-HUB-sort");
            put("12", "12-Customs-clearance-process");
            put("13", "13-Delivered");
            put("14", "14-Delivery-attempt-not-successful");
            put("15", "15-Drivers-pick-up");
            put("17", "17-Export/import-cleared");
            put("18", "18-Additional-information");
            put("20", "20-Loading");
            put("23", "23-Delivered-to-parcelshop");
        }
    };

    public static HashMap<String, String> getScanCodes() {
        return SCAN_CODES;
    }

    public static String getRandomScanCode() {
        Random generator = new Random();
        Object[] keys = SCAN_CODES.keySet().toArray();
        return (String) keys[generator.nextInt(keys.length)];
    }

}
