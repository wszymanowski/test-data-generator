package providers;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Wiktor Szymanowski
 * Date: 29.03.2020
 */
public class Date {

    public static String getTodaysDate(){
        ZonedDateTime now = ZonedDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        return dtf.format(now);
    }

    public static String getTime(){
        ZonedDateTime now = ZonedDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmmss");
        return dtf.format(now);
    }
}
