package providers;

import models.shipment.Shipment;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class ShipmentFactory {

    public Shipment deserializeXml(File xml) {

        Shipment shipment = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Shipment.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            shipment =(Shipment) jaxbUnmarshaller.unmarshal(xml);
        } catch (JAXBException e1) {
            e1.printStackTrace();
        }

        return shipment;
    }
}





