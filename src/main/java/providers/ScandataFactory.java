package providers;

import models.shipment.Shipment;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class ScandataFactory {

    String today = Date.getTodaysDate();
    String time = Date.getTime();
    String scanCode = "03"; // move to config
    String unitId = "0560"; // move to config or take from xml
    //    String depot = vars.get("unitIdTmp");
    String countryParam = "BE"; // move to Config
    String countryIsoCode = "428"; // iso map
    // Add eventless condition
    String lorryCode = "990";

    String scandataFilename = "SCANDATA."+scanCode+".PG0010201900"+ time +"." + today+".csv";
    String scandataSemFilename = scandataFilename + ".sem";

    String scandataHeader = "#FILE;depot"+unitId+";N00;"+today+";"+time+";1234;\n#VERSION: r2016-10.151465;\n#DEF;SSW:SA"+scanCode+";DEPOT;SDATE;STIME;PARCELNO;COUNTRY;TPCODE;SERVICE;TROUTE;LORRY;";



//    public void createScandata(Shipment shipment, ArrayList parcelNumbers,  LinkedHashMap<String, String> parcelNumbersAndScanCodes){
    public void createScandata(Shipment shipment, LinkedHashMap<String, String> parcelNumbersAndScanCodes){

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("src/main/resources/generated_parcels/" + scandataSemFilename, "UTF-8");
            writer = new PrintWriter("src/main/resources/generated_parcels/" + scandataFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.println(scandataHeader);
//        for (int i = 0; i < parcelNumbersAndScanCodes.size(); i++) {
        for (String key : parcelNumbersAndScanCodes.keySet()) {
            writer.print("SA"+parcelNumbersAndScanCodes.get(key)+";"+shipment.getOprShipmentVO().getOprShipmentAddInfoVO().getRealDepotId()+";"+today+";"+time+";"+key+";"+countryIsoCode+";"+shipment.getOprShipmentVO().getSenderZipCode() +";101;"+unitId +";"+lorryCode+";" + "\n");
        }
        writer.print("#END;1234;");
        writer.close();
    }
}
