package providers.shpnotcus;

import com.github.javafaker.Faker;
import models.shpnotcus.Receiver;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class ReceiverFactory {

    private Faker faker;
    private Receiver receiver;

    public ReceiverFactory (){
        faker = new Faker();
        receiver = new Receiver();
        receiver.setRNAME1(faker.name().firstName() + " " + faker.name().lastName());
        receiver.setRSTREET(faker.address().streetName());
        receiver.setRPROPNUM(faker.random().nextInt(1,100).toString());
        receiver.setRADD2("THIS IS TEST SHIPMENT");
//        receiver.setRDEPARTMENT(rDepartment);//lack in tmp
        receiver.setRCOUNTRYCODE("428");
        receiver.setRZIPCODE("1005");
        receiver.setRTOWN(faker.address().cityName());
//        receiver.setRCONTACT(rContact); //lack in tmp
        receiver.setRCONTACTPHO1("+48" + faker.random().nextInt(100000000, 999999999));
        receiver.setREMAIL(faker.name().username() + faker.random().nextInt(1, 100) + "@com.pl");
    }

    public Receiver getReceiver() {
        return receiver;
    }
}
