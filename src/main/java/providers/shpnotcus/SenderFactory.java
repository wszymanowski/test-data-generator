package providers.shpnotcus;

import com.github.javafaker.Faker;
import models.shpnotcus.Sender;

/**
 * Created by Wiktor Szymanowski
 * Date: 31.03.2020
 */
public class SenderFactory {

    private Faker faker;
    private Sender sender;

    public SenderFactory (String accountNumber){
        faker = new Faker();
        sender = new Sender();
        sender.setSCUSTACCNUMBER(accountNumber);
        sender.setSCOMPNAME("PTDGTMP");
        sender.setSUNIQCUSTID("1234567778");
        sender.setSSTREET(faker.address().streetName());
        sender.setSPROPNUM(faker.random().nextInt(1,100).toString());
        sender.setSDEPARTMENT(faker.address().cityName());
        sender.setSCOUNTRYCODE("428");
        sender.setSZIPCODE("1005");
        sender.setSTOWN(faker.address().cityName());
    }

    public Sender getSender() {
        return sender;
    }
}
