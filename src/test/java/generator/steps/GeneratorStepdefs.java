package generator.steps;

import cucumber.api.java.en.Given;
import models.shipment.Shipment;
import providers.ScandataFactory;
import providers.ShipmentFactory;
import providers.ShpnotcusFactory;
import providers.StatusdataFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Wiktor Szymanowski
 * Date: 28.03.2020
 */
public class GeneratorStepdefs {

    File file = new File("src/main/resources/xmls/saveShipmentP2.xml");

    @Given("^Generate parcel files$")
    public void generateParcelFiles() {

        // get right contract information from xml | return shipment object
        Shipment shipment = new ShipmentFactory().deserializeXml(file);

        // get shpnotcus | generate shpnotcus
        ArrayList parcelNumbers = new ShpnotcusFactory().createShpnotcus(shipment, 50);

        // get statusdata | generate statusdata
        new StatusdataFactory().createStatusdata(shipment, parcelNumbers);

        // get scandata | generate scandata

        // send shpnotcus to ftp geodata

        // send shpnotcus to ftp dpd register

        // check gtts display

        // send statusdata to ftp geodata

        //send scandata to ftp dpd register
    }

    @Given("^Generate parcel files with fake data$")
    public void generateParcelFilesWithFakeData() {
        // get right contract information from xml | return shipment object
        Shipment shipment = new ShipmentFactory().deserializeXml(file);

        // get shpnotcus | generate shpnotcus
        ArrayList parcelNumbers = new ShpnotcusFactory().createShpnotcusWithFakeData(shipment, 3);

        // get statusdata | generate statusdata
        new StatusdataFactory().createStatusdata(shipment, parcelNumbers);
    }


    @Given("^Generate parcel files with fake data separate$")
    public void generateParcelFilesWithFakeDataSeparate() {
        // get right contract information from xml | return shipment object
        Shipment shipment = new ShipmentFactory().deserializeXml(file);

        // get shpnotcus | generate shpnotcus
        ArrayList parcelNumbers = new ShpnotcusFactory().createDefaultShpnotcus(shipment, 50);

        // get statusdata | generate statusdata
        LinkedHashMap<String, String> parcelNumbersAndScanCodes = new StatusdataFactory().createStatusdata(shipment, parcelNumbers);

        // get scandata | generate scandata
        new ScandataFactory().createScandata(shipment, parcelNumbersAndScanCodes);

    }
}
